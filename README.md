# objects

collection of object stimulus files used in many experiments

to use in an experiment, call

`git submodule add \
  git@gitlab.com:psadil/objects.git \
  stims/objects`

creating a [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) called `objects` as a subdirectory of the folder `stims`

there are two advantages to setting these stimulus files in a submodule

1. the folder is displayed with an associated SHA string, so you know exactly
which version you have installed.
2. It becomes easier to ensure that the most recent (read: cleanest stims, correct
  csvs) are being used in new experiments.

## uses

The different `.csv` files each have different purposes

 - `names.csv` includes a lookup table of names that could be marked as accurate in a free recall setting. This includes many ways of misspelling each word, alternate names, partial matches; the goal is to use an algorithm like the Damerau-Levenshtein distance and have a match of 0 with the raw data that get marked as correct.
 - `name_suggestions.csv` like `names.csv`, but only two columns. This is useful when in a forced-choice setting, like use of autocomplete. First column is more general name, later columns are aliases for whatever is in the first name. Because of this, the first column is repeated throughout. The goal is that an experiment would only record responses that look like values in the first column, although a participant might provide names from one of the other columns.
 - `pairs.csv` not about naming of object but about naming of image files. This corresponds to the two objects that comprise the pair of a 2AFC decision.
