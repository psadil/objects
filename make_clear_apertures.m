

root = pwd;
apertureDir = fullfile(root,'png');
newDir = fullfile(root,'apertures_clear');
pairs = readtable('pairings.csv');

wholesFile = arrayfun(@(x)...
    dir(fullfile(apertureDir,['object-', sprintf('%03d',x),'_*', 'ap-0','.png'])),...
    pairs.pair1, 'UniformOutput',false);
[wholes_img, ~, wholes_alpha] = cellfun(@(x) imread(fullfile(apertureDir,x.name)), wholesFile, 'UniformOutput',false);


for ap = 1:3
    apertures = arrayfun(@(x)...
        dir(fullfile(apertureDir,['object-', sprintf('%03d',x),'_*',sprintf('ap-%01d',ap),'.png'])),...
        pairs.pair1, 'UniformOutput',false);
    
    [~, ~, alpha] = cellfun(@(x) imread(fullfile(apertureDir,x.name)), apertures, 'UniformOutput',false);
    
    alpha_out = cellfun(@(x,y) x .* y, wholes_alpha, alpha, 'UniformOutput', false);
    
    cellfun(@(x,y,z) imwrite(x,fullfile(newDir,y.name), 'Alpha', z), ...
        wholes_img, apertures, alpha_out, 'UniformOutput', false);
    
end


