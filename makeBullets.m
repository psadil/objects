

root = pwd;

bulletDir = fullfile(root,'bullets');
apertureDir = fullfile(root,'png');

pairs = readtable('pairings.csv');

apertures1 = arrayfun(@(x)...
    dir(fullfile(apertureDir,['object-', sprintf('%03d',x),'_*','ap-1','.png'])),...
    pairs.pair1, 'UniformOutput',false);

[~, ~, alpha1] = cellfun(@(x) imread(fullfile(apertureDir,x.name)), apertures1, 'UniformOutput',false);

alpha1_inv = cellfun(@(x) uint8(x==0), alpha1, 'UniformOutput',false);

wholesFile = arrayfun(@(x)...
    dir(fullfile(apertureDir,['object-', sprintf('%03d',x),'_*', 'ap-0','.png'])),...
    pairs.pair1, 'UniformOutput',false);
[wholes_img, ~, wholes_alpha] = cellfun(@(x) imread(fullfile(apertureDir,x.name)), wholesFile, 'UniformOutput',false);

bullet = cellfun(@(x,y) x.*y, wholes_img, alpha1_inv, 'UniformOutput', false);

alpha_out = cellfun(@(x,y) x.*y, wholes_alpha, alpha1_inv, 'UniformOutput', false);

cellfun(@(x,y,z) imwrite(x,fullfile(bulletDir,y.name), 'Alpha', z), ...
    bullet, apertures1, alpha_out, 'UniformOutput', false);

